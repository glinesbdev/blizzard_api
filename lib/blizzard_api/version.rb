# frozen_string_literal: true

module BlizzardApi
  # Gem version
  VERSION = '0.2.4'
end
